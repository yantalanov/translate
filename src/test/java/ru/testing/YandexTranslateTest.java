package ru.testing;

import org.junit.jupiter.api.DisplayName;

import ru.testing.entities.Translate;
import ru.testing.gateway.YandexTranslate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class YandexTranslateTest {
    private static final String FOLDER_ID = "b1gleuf6nif6qje5iec3";
    private static final String TARGET_LANGUAGE_RU = "ru";

    @Test
    @DisplayName("������� Hello World")
    public void getTranslations(){
        YandexTranslate yandexTranslate = new YandexTranslate();
        Translate translate = yandexTranslate.getTranslate(FOLDER_ID, "Hello World!", TARGET_LANGUAGE_RU);
        Assertions.assertEquals(translate.getTranslations().get(0).getDetectedLanguageCode(), "en");
        Assertions.assertEquals(translate.getTranslations().get(0).getText(), "���� ������!");

    }
}

















