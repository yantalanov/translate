package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import ru.testing.entities.Translate;



@Slf4j
public class YandexTranslate {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "t1.9euelZqMx8iex4-SzMfKz5WPlIueje3rnpWakM6ZkpOOy5HHyJnLzonH" +
            "icjl8_dlLxN2-e8BMwcO_d3z9yVeEHb57wEzBw79.Lzr0GYkZuHYfwByeIMv7adCNUOG6PcFNclrGIjR1pzropMn3" +
            "R670ZN9Ft6d5kiIz3dAWNw7gLhiYxaDT4ekvAA";

    @SneakyThrows
    public Translate getTranslate(String folderId, String text, String targetLanguage) {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Accept", "*/*")
                .header("Authorization", "Bearer " + TOKEN)
                .body("{\n" +
                        "    \"folderId\": \"" + folderId + "\",\n" +
                        "    \"texts\": [\"" + text + "\"],\n" +
                        "    \"targetLanguageCode\": \"" + targetLanguage + "\"\n" +
                        "}")
                .asString();
        String strResponse = response.getBody();
        log.info("response: " + strResponse);
        return gson.fromJson(strResponse, Translate.class);
    }
}























